<?php
header('Cache-Control: no cache'); 
session_cache_limiter('private_no_expire');
session_start();
include 'install.php';
include 'logout.php';
include 'base.php';
include 'admin.php';
?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8" />
  <title>Organic</title>
  <link rel="stylesheet" type="text/css" href="home.css">
</head>

<body>
  <div id="head">
      <h1>Organic</h1>
    </a>
  </div>

  <div id="menu">
    <ul>
      <li><a href="boutique.php?page=boutique">Boutique</a></li>
<?php
if (isset($_SESSION['login']))
{
    ?>
    <li><a href='mon_compte.php'>Mon Compte</a></li>
    <li><a href=''>Deconnexion</a></li>
    <?php
}
else
{
    echo"<li><a href='register.php'>Inscription</a></li>";
    echo"<li><a href='login.php'>Connexion</a></li>";
}
?>
      <li><a href="panier.php">Panier<?php
        if (!$_SESSION['nb_articles'])
            $_SESSION['nb_articles'] = 0;
        echo "(".$_SESSION['nb_articles'].")";?>

        </a></li>

      <?php
if (isset($_SESSION['groupe']) && $_SESSION['groupe'] == "admin")
	echo"<li><a href='admin.php'>Admin</a></li>";
?>
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />

    </ul>
  </div>
  <figure>
    <a href="boutique.php?page=each"><img src="img/heart.png"></a>
    <p style="color: white">Organes Uniques</p>
    <br />
  </figure>
  <figure>
    <a href="boutique.php?page=pair"><img src="img/eye.png" /></a>
    <p style="color: white">Organes doubles</p>
    <br />
  </figure>
  </div>
</body>
<footer>
</footer>

</html>
