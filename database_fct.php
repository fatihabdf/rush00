<?php
//ajout user to mysql
function ft_add_user($login, $password, $bdd){
  if (!isset($login) || !isset($password) || $login == '' || $password == '')
    return 0;
  $login = htmlspecialchars(trim($login));
  $password = hash('whirlpool', $password);
  $r_parse = "SELECT login FROM rush00.user";
  if (($parse = mysqli_query($bdd, $r_parse)) == TRUE){
    while($data = mysqli_fetch_assoc($parse)){
      if($data['login'] == $login)
        return 0;
    }
    $r_add_user = " INSERT INTO rush00.user(login, password)
                    VALUES ('$login', '$password') ";
    if (mysqli_query($bdd, $r_add_user) == TRUE)
      return 1;
  }
  return 0;
}
//verif user dans mysql00
function ft_verif_user($login, $password, $bdd){
  if (!isset($login) || !isset($password))
    return 0;
  $r_parse = "SELECT * FROM rush00.user";
  if (($parse = mysqli_query($bdd, $r_parse)) == TRUE){
    while($data = mysqli_fetch_assoc($parse)){
      if($data['login'] == $login && $data['password'] == $password)
        return 1;
      }
  }
  else
    return 0;
}
//ajout produit admin
function ft_add_product($name, $prix, $type, $bdd){
  if (isset($_SESSION['log_on_user']) && $_SESSION['groupe'] == 'admin'){
    if (!isset($name) || !isset($prix) || !isset($type) || is_numeric($prix) != TRUE)
      return 0;
    $name = trim($name);
    $r_parse = " INSERT INTO rush00.prod(name, prix, type)
                 VALUES ('$name','$prix','$type');";
    if (($parse = mysqli_query($bdd, $r_parse)) == TRUE)
      return 1;
  }
}
//supression produit
function ft_delete_product($name, $bdd){
  if (!isset($name))
    return 0;
  $r_delete = 'DELETE FROM rush00.prod
               WHERE name="'.$name.'";';
  if ((mysqli_query($bdd, $r_delete)) == TRUE)
    return 1;
}

function ft_delete_user($login, $bdd){
  if (!isset($login))
    return 0;
  $r_delete = 'DELETE FROM rush00.user
               WHERE login="'.$login.'";';
  if ((mysqli_query($bdd, $r_delete)) == TRUE)
    return 1;
}
 ?>
